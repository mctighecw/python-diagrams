from diagrams import Cluster, Diagram, Edge

from diagrams.custom import Custom
from diagrams.onprem.network import Nginx
from diagrams.programming.framework import React
from diagrams.programming.language import Python
from diagrams.onprem.database import PostgreSQL
from diagrams.onprem.inmemory import Redis
from diagrams.onprem.queue import RabbitMQ

user_icon = "../assets/client.png"
custom_edge = Edge(color="grey50")

with Diagram("Advanced Architecture", show=False, direction="LR", filename="advanced_architecture", outformat="png") as dia:
    user = Custom("User", user_icon)

    with Cluster("Docker network"):
        with Cluster("Frontend"):
            react = React("React app")
            nginx = Nginx("NGINX server")

        with Cluster("Backend"):
            flask = Python("Flask server")
            services = [PostgreSQL("Database"),
                Redis("Redis cache"),
                RabbitMQ("Message queue")]

        web_proxy = Nginx("Web proxy (SSL/TLS)")

        nginx >> custom_edge << react
        nginx >> custom_edge << flask >> custom_edge << services
        nginx >> custom_edge << web_proxy

    user >> custom_edge << nginx
dia
