from diagrams import Cluster, Diagram

from diagrams.programming.framework import React
from diagrams.programming.language import Python
from diagrams.onprem.database import PostgreSQL
from diagrams.onprem.inmemory import Redis

with Diagram("Basic Architecture", show=False, filename="basic_architecture", outformat="png") as dia:
    react = React("React frontend")

    with Cluster("Backend"):
        flask = Python("Flask server")
        services = [PostgreSQL("Database"),
                Redis("Redis cache")]

    react >> flask >> services >> flask >> react
dia
