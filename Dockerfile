FROM jupyter/minimal-notebook

USER root
WORKDIR /usr/src/notebook

COPY ./assets ./assets

RUN apt-get update && \
  apt-get install -qy \
  graphviz

RUN mkdir workspace
RUN pip install diagrams

CMD ["jupyter", "notebook", "--notebook-dir=workspace", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
