# README

Generate computer architecture diagrams with a Python package inside a [Jupyter Notebook](https://jupyter.org) running in Docker.

## App Information

App Name: python-diagrams

Created: February 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/python-diagrams)

## Tech Stack

- Python 3.8.6 (current version included in _Jupyter Notebook_ image)
- [Minimal Jupyter Notebook Image](https://hub.docker.com/r/jupyter/minimal-notebook)
- [Diagrams](https://diagrams.mingrammer.com)
- Docker

## To Run

1. Build network with `docker-compose`

```
$ docker-compose up -d --build
```

2. Check container log and copy Notebook token

```
$ docker logs python-diagrams_notebook_1

> http://127.0.0.1:8888/?token=NOTEBOOK_TOKEN
```

3. Go to `localhost:8888`, enter token, and start Notebook

## Misc

Check Python version used (in _Jupyter Notebook_ field)

```
import sys
print(sys.version)
```

Last updated: 2024-12-21
